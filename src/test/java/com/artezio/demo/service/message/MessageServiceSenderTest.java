package com.artezio.demo.service.message;

import com.artezio.demo.LoginApplication;
import com.artezio.demo.config.RabbitProperties;
import com.artezio.demo.dto.Message;
import com.artezio.demo.dto.UserDto;
import com.artezio.demo.dto.enums.MessageStatus;
import com.artezio.demo.entity.User;
import com.artezio.demo.repository.UserRepository;
import com.artezio.demo.service.map.UserMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MessageServiceSenderTest {
    @Autowired
    private MessageServiceSender sender;
    @MockBean
    private RabbitTemplate rabbitTemplateMock;
    @MockBean
    private UserRepository repository;
    @Autowired
    private RabbitProperties prop;
    @Autowired
    private UserMapper mapper;

    @Before
    public void setup() {
        sender = new MessageServiceSender(repository, mapper, rabbitTemplateMock, prop);
    }

    @Test
    @Transactional
    public void createUser() {
        User user = new User().setEmail("email").setLogin("login").setPassword("password").setName("name");
        when(repository.findAllByStatus(MessageStatus.CREATED, PageRequest.of(0, 10))).thenReturn(Collections.singletonList(user));

        ArgumentCaptor<Message<UserDto>> argumentCaptor = ArgumentCaptor.forClass(Message.class);

        doAnswer(invocation -> {
            Message<UserDto> value = argumentCaptor.getValue();
            assertNotNull(value);

            assertNotNull(value.getUser());
            UserDto dto = value.getUser();
            assertEquals(user.getEmail(), dto.getEmail());
            assertEquals(user.getLogin(), dto.getLogin());
            assertEquals(user.getName(), dto.getName());

            return null;
        }).when(rabbitTemplateMock).convertAndSend(prop.getTopicSend(), prop.getKeySend(), argumentCaptor);

        sender.sendMessage();
    }
}