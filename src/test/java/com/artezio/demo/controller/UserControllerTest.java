package com.artezio.demo.controller;

import com.artezio.demo.dto.UserDto;
import com.artezio.demo.dto.UserForm;
import com.artezio.demo.service.user.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {

    @Autowired
    protected MockMvc mockMvc;
    @MockBean
    protected UserService userService;

    @Autowired
    protected ObjectMapper objectMapper;

    @Test
        //по сути тест не несет смысловой нагрузки - т.к. замокан, для полного цикла нужно прописать @Dataset
        // - чтобы проверить. При наличии датасета -  тест должен был бы выглядеть так :
    void saveUser() throws Exception {
        UserForm user = new UserForm().setLogin("test").setName("notFound").setEmail("email@test.ru");
        when(userService.save(any(UserDto.class))).thenReturn(user);

        MockHttpServletRequestBuilder content = MockMvcRequestBuilders.post("/user/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user));

        mockMvc.perform(content)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.login").value("test"))
                .andExpect(jsonPath("$.name").value("notFound"))
                .andExpect(jsonPath("$.email").value("email@test.ru"));
    }
}