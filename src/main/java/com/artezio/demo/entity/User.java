package com.artezio.demo.entity;

import com.artezio.demo.dto.enums.ApprovalStatus;
import com.artezio.demo.dto.enums.MessageStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "user")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class User {
    // для упрощения работа выбран LONG, а не UUID
    @Id
    @GeneratedValue
    private Long id;

    // добавление уникальности логина юзера
    // в зависимости от ТЗ - можно проставлять идентификатор (unique = true)
    @Column
    private String login;

    @Column
    private String password;

    @Column
    private String email;

    @Column
    private String name;

    // по-хорошему, надо бы три этих поля вынести в отдельную таблицу UserSendInfo
    // со связью один к одному и одним и тем же ID

    /**
     * Статус отправки данных пользователя в стороннюю шину
     */
    @Column
    @Enumerated(EnumType.STRING)
    MessageStatus status;

    /**
     * Статус отправки письма пользователя с одобрением
     */
    @Column
    boolean emailSend;

    /**
     * Статус апрува пользователя
     */
    @Column
    @Enumerated(EnumType.STRING)
    ApprovalStatus approvalStatus;
}
