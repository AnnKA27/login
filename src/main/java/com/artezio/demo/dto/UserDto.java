package com.artezio.demo.dto;

import com.artezio.demo.dto.enums.MessageStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;

@Data
@Accessors(chain = true)
public class UserDto {
    @Null(message = "This field should be empty")
    private Long id;

    @NotBlank
    private String login;

    @NotBlank
    private String password;

    @Email
    @NotBlank
    private String email;

    private String name;

    @Null(message = "This field should be empty")
    private MessageStatus status;
}
