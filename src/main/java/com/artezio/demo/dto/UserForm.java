package com.artezio.demo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserForm {
    private String login;

    private String email;

    private String name;
}
