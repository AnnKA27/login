package com.artezio.demo.dto;

import com.artezio.demo.dto.enums.ApprovalStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class Message<T> implements Serializable {
    private T user;
    private ApprovalStatus approvalStatus;
    private String comments;

    public Message(T user) {
        this.user = user;
    }

    // обязательный пустой конструктор для Json Converter'a
    public Message() {

    }
}
