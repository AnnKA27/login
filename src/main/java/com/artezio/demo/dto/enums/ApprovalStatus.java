package com.artezio.demo.dto.enums;

/**
 * Статус одобрения пользователя от стороннего сервиса
 */
public enum ApprovalStatus {
    FAILED,
    SUCCESSFUL
}
