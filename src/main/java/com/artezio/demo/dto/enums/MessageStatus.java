package com.artezio.demo.dto.enums;

public enum MessageStatus {
    CREATED,
    SENT,
    RECEIVED
}
