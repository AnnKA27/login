package com.artezio.demo.controller;

import com.artezio.demo.dto.UserDto;
import com.artezio.demo.dto.UserForm;
import com.artezio.demo.service.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/save")
    public ResponseEntity<UserForm> saveUser(@RequestBody @Valid UserDto user) {
        return ResponseEntity.ok(userService.save(user));
    }
}
