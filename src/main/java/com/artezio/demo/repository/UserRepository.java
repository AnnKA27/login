package com.artezio.demo.repository;

import com.artezio.demo.dto.enums.MessageStatus;
import com.artezio.demo.entity.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAllByStatus(MessageStatus status, Pageable page);

    List<User> findAllByStatusAndEmailSendFalse(MessageStatus status, Pageable page);

    Optional<User> findByEmail(String email);

}
