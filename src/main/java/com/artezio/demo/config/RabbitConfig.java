package com.artezio.demo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Declarables;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class RabbitConfig {

    private final RabbitProperties prop;

    @Bean
    public MessageConverter jsonMessageConverter() {
        ObjectMapper objectMapper = new ObjectMapper();
        return new Jackson2JsonMessageConverter(objectMapper);
    }

    //т.к. у нас в любой момент может что-либо отвалиться- использум три очереди:
    // первая -на отправку данных в сторонний сервис
    // вторая- сторонний сервис отправляет нам эти данные обратно
    // третья - отправка email, где также все может сломаться
    // если опасаемся завала очередей- можно прописать dead-letter-queue и retry
    @Bean
    public Declarables topicBindings() {
        Queue sendApproval = new Queue(prop.getQueueSend(), false);
        Queue receivedApproval = new Queue(prop.getQueueReceived(), false);
        Queue sendEmail = new Queue(prop.getQueueEmail(), false);

        TopicExchange topicSend = new TopicExchange(prop.getTopicSend());
        TopicExchange topicReceived = new TopicExchange(prop.getTopicReceived());
        TopicExchange topicEmail = new TopicExchange(prop.getTopicEmail());

        return new Declarables(
                sendApproval, receivedApproval, sendEmail,
                topicSend, topicReceived, topicEmail,

                BindingBuilder
                        .bind(sendApproval)
                        .to(topicSend).with(prop.getKeySend()),
                BindingBuilder
                        .bind(receivedApproval)
                        .to(topicReceived).with(prop.getKeyReceived()),
                BindingBuilder
                        .bind(sendEmail)
                        .to(topicEmail).with(prop.getKeyEmail())
        );
    }
}
