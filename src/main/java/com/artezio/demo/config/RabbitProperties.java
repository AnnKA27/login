package com.artezio.demo.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@Component
public class RabbitProperties {

    @Value("${queue.send}")
    private String queueSend;

    @Value("${queue.received}")
    private String queueReceived;

    @Value("${queue.email}")
    private String queueEmail;

    @Value("${topic.send}")
    private String topicSend;

    @Value("${topic.received}")
    private String topicReceived;

    @Value("${topic.email}")
    private String topicEmail;

    @Value("${key.send}")
    private String keySend;

    @Value("${key.received}")
    private String keyReceived;

    @Value("${key.email}")
    private String keyEmail;
}

