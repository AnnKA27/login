package com.artezio.demo.service.email;

import com.artezio.demo.dto.Message;
import com.artezio.demo.dto.UserDto;
import com.artezio.demo.dto.enums.MessageStatus;
import com.artezio.demo.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeoutException;

@Service
@Slf4j
@AllArgsConstructor
public class EmailSender {
    private final EmailService emailService;
    private static final String QUEUE_EMAIL = "QUEUE_EMAIL";
    private final UserRepository repository;

    @RabbitListener(queues = { QUEUE_EMAIL })
    public void receiveAndSendEmail(Message<UserDto> message) {
        try {
            UserDto user = message.getUser();
            try {

                emailService.sendEmail(user.getEmail(), message.getApprovalStatus().name());
                //т.к. мейл тоже может отвалиться в любой момент - в пользователе
                // также есть поле об отправке мейла юзеру
                repository.findById(user.getId()).ifPresent(u -> {
                    u.setEmailSend(true);
                    repository.save(u);
                });

                log.info("SENDING EMAIL " + user.getEmail() + user.getStatus());
            } catch (TimeoutException e) {
                log.error("Something went wrong. Try again later  " + e.getMessage());
                e.printStackTrace();
            }
        } catch (AmqpException ex) {
            log.error(ex.getMessage());
            ex.printStackTrace();
        }
    }

    // второй подход - если все же мейл отвалился, также в шедулере проверять ,
    // у кого сообщение в статусе получено, но емейл не отправлен
    @Scheduled(cron = "* * * * * *")
    void sendEmail() {
        repository.findAllByStatusAndEmailSendFalse(MessageStatus.RECEIVED, PageRequest.of(0, 30))
                .forEach(user -> {
                    try {
                        emailService.sendEmail(user.getEmail(), user.getStatus().name());
                    } catch (TimeoutException e) {
                        log.error(e.getMessage());
                        e.printStackTrace();
                    }
                    repository.findById(user.getId()).ifPresent(u -> {
                        u.setEmailSend(true);
                        repository.save(u);
                    });
                });
    }
}
