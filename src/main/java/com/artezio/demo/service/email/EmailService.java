package com.artezio.demo.service.email;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeoutException;

public interface EmailService {

    void sendEmail(@NotNull String email,@NotNull String Content) throws TimeoutException;
}
