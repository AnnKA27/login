package com.artezio.demo.service.map;

import com.artezio.demo.dto.UserForm;
import com.artezio.demo.dto.UserDto;
import com.artezio.demo.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

// поля ,которых нет при мапинге - будут игнорироваться
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class UserMapper {
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public abstract UserDto mapToDto(User user);

    public abstract UserForm mapToFrom(User user);

    // добавление шифрования при мапинге пользователя
    @Mapping(target = "password", source = "password", qualifiedByName = "encoder")
    public abstract User mapToEntity(UserDto userDto);

    @Named("encoder")
    String encodePassword(String raw) {
        return passwordEncoder.encode(raw);
    }
}
