package com.artezio.demo.service.user;

import com.artezio.demo.dto.UserDto;
import com.artezio.demo.dto.UserForm;

public interface UserService {

    UserForm save(UserDto userDto);
}
