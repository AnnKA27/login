package com.artezio.demo.service.user;

import com.artezio.demo.dto.UserDto;
import com.artezio.demo.dto.UserForm;
import com.artezio.demo.dto.enums.MessageStatus;
import com.artezio.demo.entity.User;
import com.artezio.demo.exceptions.NotUniqueUserException;
import com.artezio.demo.repository.UserRepository;
import com.artezio.demo.service.map.UserMapper;
import com.sun.istack.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    @Transactional
    public UserForm save(@NotNull UserDto userDto) {
        // проверка уникальности пользователя
        // в зависимости от ТЗ можно сделать это поле PK
        userRepository.findByEmail(userDto.getEmail())
                .ifPresent(user -> {
                    throw new NotUniqueUserException(user.getEmail());
                });

        User saved = userRepository.save(userMapper.mapToEntity(userDto).setStatus(MessageStatus.CREATED));
        // т.к. нет секьюрности - возвращаем класс ,где нет внутренних данных пользователя
        return userMapper.mapToFrom(saved);
    }
}
