package com.artezio.demo.service.message;

import com.artezio.demo.config.RabbitProperties;
import com.artezio.demo.dto.Message;
import com.artezio.demo.dto.UserDto;
import com.artezio.demo.dto.enums.MessageStatus;
import com.artezio.demo.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class MessageApprovalListener {
    private final UserRepository repository;
    private final RabbitTemplate rabbitTemplate;
    private final RabbitProperties prop;
    private static final String QUEUE_RECEIVED = "QUEUE_RECEIVED";

    @RabbitListener(queues = { QUEUE_RECEIVED })
    public void receivedApproval(Message<UserDto> message) {
        try {
            UserDto user = message.getUser();
            repository.findById(user.getId()).ifPresent(u -> {
                u.setStatus(MessageStatus.RECEIVED);
                u.setApprovalStatus(message.getApprovalStatus());
                repository.save(u);
                log.info("User saved with status RECEIVED: " + u.getLogin() + " " + u.getStatus());
            });
            rabbitTemplate.convertAndSend(prop.getTopicEmail(), prop.getKeyEmail(), message);
            log.info("Send MESSAGE to the EMAIL - QUEUE");
        } catch (AmqpException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }
}
