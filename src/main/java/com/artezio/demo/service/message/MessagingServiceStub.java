package com.artezio.demo.service.message;

import com.artezio.demo.config.RabbitProperties;
import com.artezio.demo.dto.Message;
import com.artezio.demo.dto.UserDto;
import com.artezio.demo.dto.enums.ApprovalStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@RequiredArgsConstructor
@Slf4j
public class MessagingServiceStub {
    private static final String QUEUE_SEND = "QUEUE_SEND";
    private final RabbitProperties prop;
    private final RabbitTemplate rabbitTemplate;

    @RabbitListener(queues = { QUEUE_SEND })
    public void receiveForApproval(Message<UserDto> message) {
        log.info("Received message for approval: " + message.toString());

        if (new Random().nextInt(100) % 2 == 0) {
            message.setApprovalStatus(ApprovalStatus.SUCCESSFUL);
        } else {
            message.setApprovalStatus(ApprovalStatus.FAILED);
        }

        try {
            rabbitTemplate.convertAndSend(prop.getTopicReceived(), prop.getKeyReceived(), message);
        } catch (AmqpException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        log.info("SEND Message with approval " + message.getApprovalStatus());
    }
}

