package com.artezio.demo.service.message;

import com.artezio.demo.config.RabbitProperties;
import com.artezio.demo.dto.Message;
import com.artezio.demo.dto.UserDto;
import com.artezio.demo.dto.enums.MessageStatus;
import com.artezio.demo.repository.UserRepository;
import com.artezio.demo.service.map.UserMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

// т.к. сторонняя шина по проверки юзеров может отвалиться в любой момент
// - лучше сделать метод ,который будет каждую минуту проверять вновь сохраненных пользователей и
// отправляить их данные в стороннюю шину по проверке ( для этого у юзера будет поле статуса )
@RequiredArgsConstructor
@Service
@Slf4j
public class MessageServiceSender {
    private final UserRepository userRepository;
    private final UserMapper mapper;
    private final RabbitTemplate rabbitTemplate;
    private final RabbitProperties prop;

    @Scheduled(cron = "* * * * * *")
    void sendMessage() {
        // здесь возвращаем всех пользователей со статусом созданные( если используется высоконагруженная
        // система- лучше использовать с пагинацией и возвращать по 30 пользователей
        List<UserDto> userDtos = userRepository.findAllByStatus(MessageStatus.CREATED,PageRequest.of(0, 30)).stream()
                .map(mapper :: mapToDto).collect(Collectors.toList());
        userDtos.forEach(user -> {
            try {
                // отправляем вновь созданных пользователей в шину для проверки
                rabbitTemplate.convertAndSend(prop.getTopicSend(), prop.getKeySend(), new Message<>(user));

                // меняем статус юзера на отправлен  и сохраняем его в базе
                user.setStatus(MessageStatus.SENT);
                userRepository.save(mapper.mapToEntity(user));

                log.info("User send for approval " + user.getLogin());
            } catch (AmqpException e) {
                log.error(e.getMessage());
                e.printStackTrace();
            }
        });
    }
}
