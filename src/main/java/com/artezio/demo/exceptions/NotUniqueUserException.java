package com.artezio.demo.exceptions;

import java.text.MessageFormat;

public class NotUniqueUserException extends RuntimeException {
    private static final String MESSAGE = "User with email: {0} already exists";

    public NotUniqueUserException(String email) {
        super(MessageFormat.format(MESSAGE, email));
    }
}
